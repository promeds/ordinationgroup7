package service;

import ordination.*;
import storage.Storage;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;
import java.util.List;

public class Service {

    /**
     * Hvis startDato er efter slutDato kastes en IllegalArgumentException og ordinationen oprettes ikke
     * @return opretter og returnerer en PN ordination.
     */
    public static PN opretPNOrdination(LocalDate startDen, LocalDate slutDen,
                                       Patient patient, Laegemiddel laegemiddel, double antal) throws IllegalArgumentException{
        if (checkStartFoerSlut(startDen, slutDen)){
            throw new IllegalArgumentException();
        }
        else if (antal <= 0){
            throw new IllegalArgumentException("antal kan ikke være 0 eller mindre");
        }
        else{
            PN pn = new PN(startDen, slutDen, laegemiddel, antal);
            patient.addOrdination(pn);
            return pn;
        }
    }

    /**
     * Opretter og returnerer en DagligFast ordination. 
     * Hvis startDato er efter slutDato kastes en IllegalArgumentException og 
     * ordinationen oprettes ikke
     */
    public static DagligFast opretDagligFastOrdination(LocalDate startDen,
                                                       LocalDate slutDen, Patient patient, Laegemiddel laegemiddel,
                                                       double morgenAntal, double middagAntal, double aftenAntal,
                                                       double natAntal) throws IllegalArgumentException{
        if (checkStartFoerSlut(startDen, slutDen)) {
            throw new IllegalArgumentException();
        } else {
            DagligFast dagligFast = new DagligFast(startDen, slutDen, laegemiddel, morgenAntal, middagAntal, aftenAntal, natAntal);
            patient.addOrdination(dagligFast);
            return dagligFast;
        }

    }

    /**
     *  Opretter og returnerer en DagligSkæv ordination.
     *  Hvis startDato er efter slutDato kastes en IllegalArgumentException og 
     *  ordinationen oprettes ikke
     */
    public static DagligSkaev opretDagligSkaevOrdination(LocalDate startDen,
                                                         LocalDate slutDen, Patient patient, Laegemiddel laegemiddel,
                                                         LocalTime[] klokkeSlet, double[] antalEnheder) throws IllegalArgumentException {
        if (checkStartFoerSlut(startDen, slutDen)) {
            throw new IllegalArgumentException();
        }
        else if (klokkeSlet.length <= 0 && antalEnheder.length <= 0){
            throw new IllegalArgumentException();
        }

        else {
            for (double d : antalEnheder){
                if (d == 0){
                    throw new IllegalArgumentException();
                }
            }
            DagligSkaev dagligSkaev = new DagligSkaev(startDen, slutDen, laegemiddel);
            patient.addOrdination(dagligSkaev);
            if (klokkeSlet.length != antalEnheder.length){
                throw new IllegalArgumentException("Du kan ikke have en tid uden en dosis");
            }
            for (int i = 0; i < klokkeSlet.length; i++){
                dagligSkaev.opretDosis(klokkeSlet[i], antalEnheder[i]);
            }
            return dagligSkaev;
        }

    }

    /**
     *  En dato for hvornår ordinationen anvendes tilføjes
     *  ordinationen. Hvis datoen ikke er indenfor ordinationens
     *  gyldighedsperiode kastes en IllegalArgumentException
     */
    public static void ordinationPNAnvendt(PN ordination, LocalDate dato) throws IllegalArgumentException{
        ordination.givDosis(dato);
    }

    /**
     * Den anbefalede dosis for den pågældende patient (der skal tages
     * hensyn til patientens vægt) Det er en forskellig enheds faktor
     * der skal anvendes, den er afhængigt af patientens vægt
     */
    public static double anbefaletDosisPrDoegn(Patient patient, Laegemiddel laegemiddel){
        double result;
        if (patient.getVaegt() < 25) {
            result = patient.getVaegt() * laegemiddel.getEnhePrKgPrDoegnLet();
        }
        else if (patient.getVaegt() > 120) {
            result = patient.getVaegt() * laegemiddel.getEnhePrKgPrDoegnTung();
        }
        else {
            result = patient.getVaegt() * laegemiddel.getEnhePrKgPrDoegnNormal();
        }
        return result;
    }

    /**
     * For et givent vægtinterval og et givent lægemiddel, hentes antallet af ordinationer.
     */
    public static int antalOrdinationerPrVægtPrLægemiddel(double vægtStart,
                                                          double vægtSlut, Laegemiddel laegemiddel) {
        double result = 0;
        for (Patient p : Storage.getAllPatienter()){
            if (p.getVaegt() >= vægtStart && p.getVaegt() <= vægtSlut){
                for (Ordination o : p.getOrdinationer()){
                    if (o.getLaegemiddel() == laegemiddel){
                        result++;
                    }
                }
            }
        }
        return (int) result;
    }


    public static List<Patient> getAllPatienter() {
        return Storage.getAllPatienter();
    }

    public static List<Laegemiddel> getAllLaegemidler() {
        return Storage.getAllLaegemidler();
    }

    private static boolean checkStartFoerSlut(LocalDate startDato, LocalDate slutDato) {
        if (Period.between(startDato, slutDato).isNegative()){
            return true;
        } else {
            return false;
        }
    }

    public static void createSomeObjects() {
        Storage.gemPatient(new Patient("121256-0512", "Jane Jensen", 63.4));
        Storage.gemPatient(new Patient("070985-1153", "Finn Madsen", 83.2));
        Storage.gemPatient(new Patient("050972-1233", "Hans Jørgensen", 89.4));
        Storage.gemPatient(new Patient("011064-1522", "Ulla Nielsen", 59.9));
        Storage.gemPatient(new Patient("090149-2529", "Ib Hansen", 87.7));

        Storage.gemLaegemiddel(new Laegemiddel("Pinex", 0.1, 0.15, 0.16, "Styk"));
        Storage.gemLaegemiddel(new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml"));
        Storage.gemLaegemiddel(new Laegemiddel("Fucidin", 0.025, 0.025, 0.025,
                "Styk"));
        Storage.gemLaegemiddel(new Laegemiddel("ABC", 0.01, 0.015, 0.02, "Styk"));

        opretPNOrdination(LocalDate.of(2015, 1, 1),
                LocalDate.of(2015, 1, 12), Storage.getAllPatienter().get(0),
                Storage.getAllLaegemidler().get(1), 123);

        opretPNOrdination(LocalDate.of(2015, 2, 12),
                LocalDate.of(2015, 2, 14), Storage.getAllPatienter().get(0),
                Storage.getAllLaegemidler().get(0), 3);

        opretPNOrdination(LocalDate.of(2015, 1, 20),
                LocalDate.of(2015, 1, 25), Storage.getAllPatienter().get(3),
                Storage.getAllLaegemidler().get(2), 5);

        opretPNOrdination(LocalDate.of(2015, 1, 1),
                LocalDate.of(2015, 1, 12), Storage.getAllPatienter().get(0),
                Storage.getAllLaegemidler().get(1), 123);

        opretDagligFastOrdination(LocalDate.of(2015, 1, 10),
                LocalDate.of(2015, 1, 12), Storage.getAllPatienter().get(1),
                Storage.getAllLaegemidler().get(1), 2, -1, 1, -1);

        LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40), LocalTime.of(16, 0), LocalTime.of(18, 45) };
        double[] an = { 0.5, 1, 2.5, 3 };

        opretDagligSkaevOrdination(LocalDate.of(2015, 1, 23),
                LocalDate.of(2015, 1, 24), Storage.getAllPatienter().get(1),
                Storage.getAllLaegemidler().get(2), kl, an);
    }

}
