package service;

import ordination.*;

import org.junit.Test;

import storage.Storage;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.Assert.*;

public class ServiceTest {



    @Test
    public void testAnbefaletDosisPrDoegn1() throws Exception{
        Patient p1 = new Patient("123456-1117", "p1", 24);
        Laegemiddel l1 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "ml");
        assertTrue(Service.anbefaletDosisPrDoegn(p1, l1) == 24);
    }


    @org.junit.Test
    public void testAnbefaletDosisPrDoegn2() throws Exception {
        Patient p2 = new Patient("123456-1117", "p2", 25);
        Laegemiddel l1 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "ml");
        assertTrue(Service.anbefaletDosisPrDoegn(p2,l1) == 37.5);
    }

    @Test
    public void testAnbefaletDosisPrDoegn3() throws Exception{
        Patient p3 = new Patient("123456-1117", "p3", 78);
        Laegemiddel l1 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "ml");
        assertTrue(Service.anbefaletDosisPrDoegn(p3, l1) == 117);
    }

    @Test
    public void testAnbefaletDosisPrDoegn4() throws Exception{
        Patient p4 = new Patient("123456-1117", "p4", 119);
        Laegemiddel l1 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "ml");
        assertTrue(Service.anbefaletDosisPrDoegn(p4,l1) == 178.5);
    }

    @Test
    public void testAnbefaletDosisPrDoegn5() throws Exception {
        Patient p5 = new Patient("123456-1117", "p5", 120);
        Laegemiddel l1 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "ml");
        assertTrue(Service.anbefaletDosisPrDoegn(p5, l1) == 180);
    }

    @Test
    public void testAnbefaletDosisPrDoegn6() throws Exception{
        Patient p6 = new Patient("123456-1117", "p6", 125);
        Laegemiddel l1 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "ml");
        assertTrue(Service.anbefaletDosisPrDoegn(p6, l1) == 250);
        assertEquals(250.0, Service.anbefaletDosisPrDoegn(p6,l1), 0.1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAnbefaletDosisPrDoegn7(){
        Patient p7 = new Patient("123456-1117", "p7", -74);
        Laegemiddel l1 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "ml");
        Service.anbefaletDosisPrDoegn(p7,l1);
    }

    @Test
    public void testDagligFast1() throws Exception{
        Patient p1 = new Patient("123456-1117", "x", 75);
        Laegemiddel l1 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "stk");
        DagligFast d = Service.opretDagligFastOrdination(LocalDate.of(2015, 4, 11), LocalDate.of(2015, 4, 11), p1, l1, 2, 1, 2, 1);
        assertTrue(p1.getOrdinationer().contains(d));
        assertTrue(d.getLaegemiddel() == l1);
        assertEquals(2, d.getDoser()[0].getAntal(), 0.1);
        assertEquals(1, d.getDoser()[1].getAntal(), 0.1);
        assertEquals(2, d.getDoser()[2].getAntal(), 0.1);
        assertEquals(1, d.getDoser()[3].getAntal(), 0.1);
    }

    @Test
    public void testDagligFast2() throws Exception{
        Patient p1 = new Patient("120589-0905", "x", 75);
        Laegemiddel l1 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "stk");
        DagligFast d = Service.opretDagligFastOrdination(LocalDate.of(2014, 3, 6), LocalDate.of(2015, 3, 6), p1, l1, 1, 0, 1, 2);
        assertTrue(p1.getOrdinationer().contains(d));
        assertTrue(d.getLaegemiddel() == l1);
        assertEquals(1, d.getDoser()[0].getAntal(), 0.1);
        assertEquals(0, d.getDoser()[1].getAntal(), 0.1);
        assertEquals(1, d.getDoser()[2].getAntal(), 0.1);
        assertEquals(2, d.getDoser()[3].getAntal(), 0.1);
    }

    @Test (expected = DateTimeException.class)
    public void testDagligFast3() throws Exception {
        Patient p1 = new Patient("120589-0905", "x", 75);
        Laegemiddel l1 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "stk");
        DagligFast d = Service.opretDagligFastOrdination(LocalDate.of(2015, 2, 29), LocalDate.of(2015, 3, 1), p1, l1, 1, 0, 1, 2);
    }

    @Test
    public void testOpretPN1() throws Exception {
        Patient p1 = new Patient("123456-1117", "1", 50);
        Laegemiddel l1 = new Laegemiddel("2", 1,2,3,"stk");
        PN pn = Service.opretPNOrdination(LocalDate.of(2015, 2, 25), LocalDate.of(2015, 2, 25), p1, l1, 2.5);
        assertTrue(p1.getOrdinationer().contains(pn));
        assertTrue(pn.getLaegemiddel() == l1);
        assertEquals(LocalDate.of(2015, 2, 25), pn.getStartDen());
        assertEquals(LocalDate.of(2015, 2, 25), pn.getSlutDen());
        assertEquals(2.5, pn.getAntalEnheder(), 0.1);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testOpretPN2() {
        Patient p1 = new Patient("123456-1117", "1", 50);
        Laegemiddel l1 = new Laegemiddel("2", 1,2,3,"stk");
        PN pn = Service.opretPNOrdination(LocalDate.of(2015, 2, 25), LocalDate.of(2015, 2, 24), p1, l1, 2.5);
    }

    @Test(expected = IllegalArgumentException.class)
        public void testOpretPN3() {
        Patient p1 = new Patient("123456-1117", "1", 50);
        Laegemiddel l1 = new Laegemiddel("2", 1,2,3,"stk");
        PN pn = Service.opretPNOrdination(LocalDate.of(2015, 2, 25), LocalDate.of(2015, 2, 25), p1, l1, 0);
        }

    @Test
    public void testSamletDosis1() throws Exception{
        Patient p1 = new Patient("123456-1117", "1", 50);
        Laegemiddel l1 = new Laegemiddel("2", 1,2,3,"stk");
        DagligFast d = Service.opretDagligFastOrdination(LocalDate.of(2014, 12, 29), LocalDate.of(2015, 2, 1), p1, l1, 2, 0, 2, 1);
        p1.addOrdination(d);
        assertTrue(d.samletDosis() == 175);
        assertTrue(d.doegnDosis() == 5);
    }

    @Test
    public void testSamletDosis2() throws Exception{
        Patient p1 = new Patient("123456-1117", "1", 50);
        Laegemiddel l1 = new Laegemiddel("2", 1,2,3,"stk");
        DagligFast d = Service.opretDagligFastOrdination(LocalDate.of(2015, 11, 4), LocalDate.of(2015, 11, 4), p1, l1, 1, 0, 1, 1);
        p1.addOrdination(d);
        assertTrue(d.doegnDosis() == 3);
        assertTrue(d.samletDosis() == 3);
    }

    @Test
    public void testSamletDosis3() throws Exception{
            Patient p1 = new Patient("123456-1117", "1", 50);
            Laegemiddel l1 = new Laegemiddel("2", 1,2,3,"stk");
            DagligFast d = Service.opretDagligFastOrdination(LocalDate.of(2014, 6, 3), LocalDate.of(2015, 6, 3), p1, l1, 1, 0, 2, 0);
            assertTrue(d.doegnDosis() == 3);
            assertEquals(1098, d.samletDosis(), 0.1);
    }

    @Test (expected = DateTimeException.class)
    public void testSamletDosis4() {
        Patient p1 = new Patient("123456-1117", "1", 50);
        Laegemiddel l1 = new Laegemiddel("2", 1,2,3,"stk");
        DagligFast d = Service.opretDagligFastOrdination(LocalDate.of(2015, 2 , 29), LocalDate.of(2015, 3, 1), p1, l1, 2, 0, 1, 1);
        p1.addOrdination(d);

    }

    @Test
    public void ordinationer1(){
        Patient p1 = new Patient("123456-1117", "X", 75);
        Storage.gemPatient(p1);
        Patient p2 = new Patient("123456-1113", "z", 87);
        Patient p3 = new Patient("123456-1116", "y", 107);
        Storage.gemPatient(p2);
        Storage.gemPatient(p3);

        Laegemiddel l1 = new Laegemiddel("Fucidin", 2,2,2, "stk");
        Laegemiddel l2 = new Laegemiddel("Paracetamol", 2,2,2, "stk");

        Service.opretPNOrdination(LocalDate.now(), LocalDate.now(), p1, l1, 2);
        Service.opretDagligFastOrdination(LocalDate.now(), LocalDate.now(), p1, l1, 2, 2, 2, 2);
        Service.opretPNOrdination(LocalDate.now(), LocalDate.now(), p1, l2, 5);


        Service.opretDagligFastOrdination(LocalDate.now(), LocalDate.now(), p2, l2, 2, 3, 4, 5);
        Service.opretPNOrdination(LocalDate.now(), LocalDate.now(), p2, l2, 2);
        Service.opretPNOrdination(LocalDate.now(), LocalDate.now(), p2, l1, 2);

        Service.opretDagligFastOrdination(LocalDate.now(), LocalDate.now(), p3, l2, 2, 2, 2, 2);
        Service.opretDagligFastOrdination(LocalDate.now(), LocalDate.now(), p3, l1, 2, 2, 2, 2);
        Service.opretPNOrdination(LocalDate.now(), LocalDate.now(), p3, l2, 2);
        Service.opretPNOrdination(LocalDate.now(), LocalDate.now(), p3, l1, 2);

        assertEquals(3, Service.antalOrdinationerPrVægtPrLægemiddel(60,89,l1), 0.2);



    }

    @Test
    public void ordinationer2(){
        Patient p1 = new Patient("123456-1117", "X", 75);
        Storage.gemPatient(p1);
        Patient p2 = new Patient("123456-1113", "z", 87);
        Patient p3 = new Patient("123456-1116", "y", 107);
        Storage.gemPatient(p2);
        Storage.gemPatient(p3);

        Laegemiddel l1 = new Laegemiddel("Fucidin", 2,2,2, "stk");
        Laegemiddel l2 = new Laegemiddel("Paracetamol", 2,2,2, "stk");

        Service.opretPNOrdination(LocalDate.now(), LocalDate.now(), p1, l1, 2);
        Service.opretDagligFastOrdination(LocalDate.now(), LocalDate.now(), p1, l1, 2, 2, 2, 2);
        Service.opretPNOrdination(LocalDate.now(), LocalDate.now(), p1, l2, 5);


        Service.opretDagligFastOrdination(LocalDate.now(), LocalDate.now(), p2, l2, 2, 3, 4, 5);
        Service.opretPNOrdination(LocalDate.now(), LocalDate.now(), p2, l2, 2);
        Service.opretPNOrdination(LocalDate.now(), LocalDate.now(), p2, l1, 2);

        Service.opretDagligFastOrdination(LocalDate.now(), LocalDate.now(), p3, l2, 2, 2, 2, 2);
        Service.opretDagligFastOrdination(LocalDate.now(), LocalDate.now(), p3, l1, 2, 2, 2, 2);
        Service.opretPNOrdination(LocalDate.now(), LocalDate.now(), p3, l2, 2);
        Service.opretPNOrdination(LocalDate.now(), LocalDate.now(), p3, l1, 2);

        assertEquals(4, Service.antalOrdinationerPrVægtPrLægemiddel(85, 110, l2), 0.1);
    }

    @Test
    public void ordinationer3(){
        Patient p1 = new Patient("123456-1117", "X", 75);
        Storage.gemPatient(p1);
        Patient p2 = new Patient("123456-1113", "z", 87);
        Patient p3 = new Patient("123456-1116", "y", 107);
        Storage.gemPatient(p2);
        Storage.gemPatient(p3);

        Laegemiddel l1 = new Laegemiddel("Fucidin", 2,2,2, "stk");
        Laegemiddel l2 = new Laegemiddel("Paracetamol", 2,2,2, "stk");

        Service.opretPNOrdination(LocalDate.now(), LocalDate.now(), p1, l1, 2);
        Service.opretDagligFastOrdination(LocalDate.now(), LocalDate.now(), p1, l1, 2, 2, 2, 2);
        Service.opretPNOrdination(LocalDate.now(), LocalDate.now(), p1, l2, 5);


        Service.opretDagligFastOrdination(LocalDate.now(), LocalDate.now(), p2, l2, 2, 3, 4, 5);
        Service.opretPNOrdination(LocalDate.now(), LocalDate.now(), p2, l2, 2);
        Service.opretPNOrdination(LocalDate.now(), LocalDate.now(), p2, l1, 2);

        Service.opretDagligFastOrdination(LocalDate.now(), LocalDate.now(), p3, l2, 2, 2, 2, 2);
        Service.opretDagligFastOrdination(LocalDate.now(), LocalDate.now(), p3, l1, 2, 2, 2, 2);
        Service.opretPNOrdination(LocalDate.now(), LocalDate.now(), p3, l2, 2);
        Service.opretPNOrdination(LocalDate.now(), LocalDate.now(), p3, l1, 2);

        assertEquals(3, Service.antalOrdinationerPrVægtPrLægemiddel(87, 107, l1));
    }

   @Test
    public void givDosis1(){
       Patient p1 = new Patient("123456-1117", "X", 75);
       Laegemiddel l1 = new Laegemiddel("Fucidin", 2,2,2, "stk");
       PN pn = Service.opretPNOrdination(LocalDate.of(2015, 1,2), LocalDate.of(2015, 1, 12), p1, l1, 2);
       pn.givDosis(LocalDate.of(2015, 1, 2));
   }

    @Test
    public void givDosis2(){
        Patient p1 = new Patient("123456-1117", "X", 75);
        Laegemiddel l1 = new Laegemiddel("Fucidin", 2,2,2, "stk");
        PN pn = Service.opretPNOrdination(LocalDate.of(2015, 1,2), LocalDate.of(2015, 1, 12), p1, l1, 2);
        pn.givDosis(LocalDate.of(2015, 1, 12));
    }

    @Test(expected = IllegalArgumentException.class)
    public void givDosis3(){
        Patient p1 = new Patient("123456-1117", "X", 75);
        Laegemiddel l1 = new Laegemiddel("Fucidin", 2,2,2, "stk");
        PN pn = Service.opretPNOrdination(LocalDate.of(2015, 1,2), LocalDate.of(2015, 1, 12), p1, l1, 2);
        pn.givDosis(LocalDate.of(2015, 1, 13));
    }

    @Test(expected = IllegalArgumentException.class)
    public void givDosis4(){
        Patient p1 = new Patient("123456-1117", "X", 75);
        Laegemiddel l1 = new Laegemiddel("Fucidin", 2,2,2, "stk");
        PN pn = Service.opretPNOrdination(LocalDate.of(2015, 1,2), LocalDate.of(2015, 1, 12), p1, l1, 2);
        pn.givDosis(LocalDate.of(2015, 1, 1));
    }

    @Test
    public void pnSamletDoegn1(){
        Patient p1 = new Patient("123456-1117", "X", 75);
        Laegemiddel l1 = new Laegemiddel("Fucidin", 2,2,2, "stk");
        PN pn = Service.opretPNOrdination(LocalDate.of(2015, 1,2), LocalDate.of(2015, 1, 12), p1, l1, 2);
        assertEquals(0,pn.samletDosis(), 0.1);
    }

    @Test
    public void pnSamletDoegn2(){
        Patient p1 = new Patient("123456-1117", "X", 75);
        Laegemiddel l1 = new Laegemiddel("Fucidin", 2,2,2, "stk");
        PN pn = Service.opretPNOrdination(LocalDate.of(2015, 1,2), LocalDate.of(2015, 1, 12), p1, l1, 2);
        pn.givDosis(LocalDate.of(2015, 1, 2));
        assertEquals(2, pn.samletDosis(), 0.1);
    }

    @Test
    public void pnSamletDoegn3(){
        Patient p1 = new Patient("123456-1117", "X", 75);
        Laegemiddel l1 = new Laegemiddel("Fucidin", 2,2,2, "stk");
        PN pn = Service.opretPNOrdination(LocalDate.of(2015, 1,2), LocalDate.of(2015, 1, 12), p1, l1, 2);
        pn.givDosis(LocalDate.of(2015, 1, 4));
        pn.givDosis(LocalDate.of(2015, 1, 2));
        assertEquals(4, pn.samletDosis(), 0.1);
        assertEquals(1.333, pn.doegnDosis(), 0.1);
    }

    @Test
    public void pnSamletDoegn4(){
        Patient p1 = new Patient("123456-1117", "X", 75);
        Laegemiddel l1 = new Laegemiddel("Fucidin", 2,2,2, "stk");
        PN pn = Service.opretPNOrdination(LocalDate.of(2015, 1,2), LocalDate.of(2015, 1, 12), p1, l1, 2);
        pn.givDosis(LocalDate.of(2015, 1, 6));
        pn.givDosis(LocalDate.of(2015, 1, 3));
        pn.givDosis(LocalDate.of(2015, 1, 12));
        assertEquals(6, pn.samletDosis(), 0.1);
        assertEquals(0.6, pn.doegnDosis(), 0.1);
    }

    @Test
    public void pnSamletDoegn5(){
        Patient p1 = new Patient("123456-1117", "X", 75);
        Laegemiddel l1 = new Laegemiddel("Fucidin", 2,2,2, "stk");
        PN pn = Service.opretPNOrdination(LocalDate.of(2015, 1,2), LocalDate.of(2015, 1, 12), p1, l1, 2);
        pn.givDosis(LocalDate.of(2015, 1, 2));
        pn.givDosis(LocalDate.of(2015, 1, 5));
        pn.givDosis(LocalDate.of(2015, 1, 2));
        pn.givDosis(LocalDate.of(2015, 1, 7));
        assertEquals(8, pn.samletDosis(), 0.1);
        assertEquals(1.333, pn.doegnDosis(), 0.1);
    }

    @Test
    public void opretSkaev1(){
        Patient p1 = new Patient("123456-1117", "1", 50);
        Laegemiddel l1 = new Laegemiddel("2", 1,2,3,"stk");
        LocalTime[] time = {LocalTime.of(13,40)};
        double[] antalEnheder = {2};
        DagligSkaev d = Service.opretDagligSkaevOrdination(LocalDate.of(2015, 2, 25), LocalDate.of(2015, 2, 25), p1, l1, time,antalEnheder);

        assertEquals(2, d.getDoser().get(0).getAntal(),0.001);
        assertTrue(d.getStartDen().equals(LocalDate.of(2015, 2, 25)));
        assertTrue(d.getSlutDen().equals(LocalDate.of(2015, 2, 25)));


    }

    @Test
    public void opretSkaev2(){
        Patient p1 = new Patient("123456-1117", "1", 50);
        Laegemiddel l1 = new Laegemiddel("2", 1,2,3,"stk");
        LocalTime[] time = {LocalTime.of(13,40), LocalTime.of(10,40)};
        double[] antalEnheder = {1,5};
        DagligSkaev d = Service.opretDagligSkaevOrdination(LocalDate.of(2015, 2, 25), LocalDate.of(2015, 2, 25), p1, l1, time,antalEnheder);

        assertEquals(1, d.getDoser().get(0).getAntal(),0.001);
        assertEquals(5, d.getDoser().get(1).getAntal(), 0.01);
        assertTrue(d.getStartDen().equals(LocalDate.of(2015, 2, 25)));
        assertTrue(d.getSlutDen().equals(LocalDate.of(2015, 2, 25)));
    }

    @Test (expected = IllegalArgumentException.class)
    public void opretSkaev3(){
        Patient p1 = new Patient("123456-1117", "1", 50);
        Laegemiddel l1 = new Laegemiddel("2", 1,2,3,"stk");
        LocalTime[] time = {LocalTime.of(13,40)};
        double[] antalEnheder = {2};
        DagligSkaev d = Service.opretDagligSkaevOrdination(LocalDate.of(2015, 2, 25), LocalDate.of(2015, 2, 24), p1, l1, time,antalEnheder);
    }

    @Test (expected = IllegalArgumentException.class)
    public void opretSkaev4(){
        Patient p1 = new Patient("123456-1117", "1", 50);
        Laegemiddel l1 = new Laegemiddel("2", 1,2,3,"stk");
        LocalTime[] time = {};
        double[] antalEnheder = {};
        DagligSkaev d = Service.opretDagligSkaevOrdination(LocalDate.of(2015, 2, 25), LocalDate.of(2015, 2, 27), p1, l1, time,antalEnheder);
    }

    @Test(expected = IllegalArgumentException.class)
    public void opretSkaev5(){
        Patient p1 = new Patient("123456-1117", "1", 50);
        Laegemiddel l1 = new Laegemiddel("2", 1,2,3,"stk");
        LocalTime[] time = {LocalTime.of(13,40)};
        double[] antalEnheder = {4, 2};
        DagligSkaev d = Service.opretDagligSkaevOrdination(LocalDate.of(2015, 2, 25), LocalDate.of(2015, 2, 27), p1, l1, time,antalEnheder);
    }

    @Test(expected = IllegalArgumentException.class)
    public void opretSkaev6(){
        Patient p1 = new Patient("123456-1117", "1", 50);
        Laegemiddel l1 = new Laegemiddel("2", 1,2,3,"stk");
        LocalTime[] time = {LocalTime.of(13,40), LocalTime.of(17,20)};
        double[] antalEnheder = {4};
        DagligSkaev d = Service.opretDagligSkaevOrdination(LocalDate.of(2015, 2, 25), LocalDate.of(2015, 2, 27), p1, l1, time,antalEnheder);
    }

    @Test (expected = IllegalArgumentException.class)
    public void opretSkaev7(){
        Patient p1 = new Patient("123456-1117", "1", 50);
        Laegemiddel l1 = new Laegemiddel("2", 1,2,3,"stk");
        LocalTime[] time = {LocalTime.of(13,40)};
        double[] antalEnheder = {0};
        DagligSkaev d = Service.opretDagligSkaevOrdination(LocalDate.of(2015, 2, 25), LocalDate.of(2015, 2, 25), p1, l1, time,antalEnheder);

    }
    
    @Test
    public void patient1(){
        Patient p = new Patient("121200-1111", "L", 50);
        assertEquals("L", p.getNavn());
        assertEquals("121200-1111", p.getCprnr());
        assertEquals(50, p.getVaegt(), 0.001);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void patient2(){
        new Patient("121200-1111", "", 50);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void patient3(){
        new Patient("121200-1111", "patient", -2);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void patient4(){
        new Patient("121200-111", "patient", 50);
    }
    
    @Test
    public void laegemiddel1(){
    	Laegemiddel l = new Laegemiddel("H3IL", 0.3, 0.4, 0.6, "ml");
    	assertEquals("H3IL", l.getNavn());
    	assertEquals(0.3, l.getEnhePrKgPrDoegnLet(), 0.001);
    	assertEquals(0.4, l.getEnhePrKgPrDoegnNormal(), 0.001);
    	assertEquals(0.6, l.getEnhePrKgPrDoegnTung(), 0.001);
    	assertEquals("H3IL", l.getNavn());
    }
    @Test(expected = IllegalArgumentException.class)
    public void Laegemiddel2(){
    	new Laegemiddel("H3IL", 0.3, 0.4, 0.6, "");
    }
    @Test(expected = IllegalArgumentException.class)
    public void Laegemiddel3(){
    	new Laegemiddel("H3IL", 0.3, 0.4, 0.0, "ml");
    }
    @Test(expected = IllegalArgumentException.class)
    public void Laegemiddel4(){
    	new Laegemiddel("H3IL", 0.3, 0.0, 0.6, "ml");
    }
    @Test(expected = IllegalArgumentException.class)
    public void Laegemiddel5(){
    	new Laegemiddel("H3IL", 0.0, 0.4, 0.6, "ml");
    }
    @Test(expected = IllegalArgumentException.class)
    public void Laegemiddel6(){
    	new Laegemiddel("", 0.3, 0.4, 0.6, "ml");
    }



}


