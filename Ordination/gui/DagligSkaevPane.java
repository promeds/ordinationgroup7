package gui;

import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.*;
import javafx.scene.text.Font;
import javafx.util.Duration;
import javafx.scene.paint.Color;



public class DagligSkaevPane extends GridPane {
    private TextField txtTimeMinut = new TextField();
    private TextField txtMaengde = new TextField();
    private Button btnOpret = new Button("Opret dosis");
    private ListView<String> listDosis = new ListView<>();
    private Label lblError = new Label();


    public DagligSkaevPane() {
        setHgap(20);
        setVgap(10);
        setGridLinesVisible(false);



        txtTimeMinut.setPromptText("TT:MM");
        txtMaengde.setPromptText("Mængde");

        HBox hbox = new HBox(8);
        hbox.getChildren().add(txtTimeMinut);
        hbox.getChildren().add(txtMaengde);
        hbox.getChildren().add(btnOpret);
        add(hbox, 0, 0);

        listDosis.setMaxHeight(100);
        lblError.setTextFill(javafx.scene.paint.Color.RED);
        add(listDosis, 0, 1);
        add(lblError, 0, 2);

        btnOpret.setOnAction(event -> opretDosis());
    }

    private void opretDosis() {
        AnimationTimer a = new AnimationTimer() {
            int i = 0;
            @Override
            public void handle(long now){
                i++;
                lblError.setFont(Font.font("Times New Roman", i));
                if (i ==30){
                    lblError.setTextFill(Color.RED);
                    // lblError.setFont(Font.font("Times New Roman", 40));
                }else if ( i == 60){
                    lblError.setTextFill(Color.GRAY);
                    //lblError.setFont(Font.font("Calibri", 20));
                    i= 0;



            }}
        };
        try{
        if (Integer.parseInt(txtMaengde.getText()) < 0){
            throw new IllegalArgumentException("Mængde skal være positiv");} else{
            a.stop();
            String dosis = txtTimeMinut.getText() + " " + txtMaengde.getText();
            listDosis.getItems().add(dosis);

            }
        } catch (IllegalArgumentException f){
            a.start();
            lblError.setText(f.getMessage());

        }


    }

    public String[] getDosisArray() {
        ObservableList<String> items = listDosis.getItems();
        return items.toArray(new String[items.size()]);
    }
}
