package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.TreeSet;

public class DagligSkaev extends Ordination{
    private Dosis dosis;
    private ArrayList<Dosis> list =  new ArrayList<>();

    public DagligSkaev(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel) {
        super(startDen, slutDen, laegemiddel);
    }

    public void opretDosis(LocalTime tid, double antal) throws IllegalArgumentException {
        dosis = new Dosis(tid, antal);
        if (list.contains(dosis)){
            throw new IllegalArgumentException("Du kan ikke oprette dosis på samme tidspunkt som der allerede er oprettet en dosis");
        } else {
            list.add(dosis);
        }
	}

    public ArrayList<Dosis> getDoser(){
        return new ArrayList<>(list);
    }

	@Override
	public double samletDosis() {
        return doegnDosis() * antalDage();
	}

	@Override
	public double doegnDosis() {
        double sum = 0;
        for (Dosis d : list){
            sum += d.getAntal();
        }
		return sum;
	}


	@Override
	public String getType() {
        return "daglig skæv";
	}


}
