package ordination;

import java.lang.reflect.Array;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligFast extends Ordination{

	private LocalTime morgen = LocalTime.of(8,00);
    private LocalTime middag = LocalTime.of(13,00);
    private LocalTime aften = LocalTime.of(18,00);
    private LocalTime nat = LocalTime.of(23,00);

    private Dosis[] dagligDosis = new Dosis[4];

	public DagligFast(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, double morgenDosis, double middagDosis, double aftenDosis, double natDosis ){
        super(startDen, slutDen, laegemiddel);
        dagligDosis[0] = new Dosis(morgen, morgenDosis);
        dagligDosis[1] = new Dosis(middag, middagDosis);
        dagligDosis[2] = new Dosis(aften, aftenDosis);
        dagligDosis[3] = new Dosis(nat, natDosis);
	}

	@Override
	public double samletDosis() {
		return super.antalDage() * doegnDosis();
	}

	@Override
	public double doegnDosis() {
		return dagligDosis[0].getAntal() + dagligDosis[1].getAntal() +
                dagligDosis[2].getAntal() + dagligDosis[3].getAntal();
	}

    public Dosis[] getDoser(){
        return dagligDosis;
    }

	@Override
	public String getType() {
		return "daglig fast";
	}
}
