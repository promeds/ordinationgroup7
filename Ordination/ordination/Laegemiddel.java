package ordination;

public class Laegemiddel {
	private String navn;
	private double enhedPrKgPrDoegnLet;   // faktor der anvendes hvis patient vejer < 25 kg
	private double enhedPrKgPrDoegnNormal;// faktor der anvendes hvis 25 kg <= patient v�gt <= 120 kg
	private double enhedPrKgPrDoegnTung;  // faktor der anvendes hvis patient v�gt > 120 kg
	private String enhed;
	
	public Laegemiddel(String navn, double enhedPrKgPrDoegnLet, double enhedPrKgPrDoegnNormal , 
	        double enhedPrKgPrDoegnTung, String enhed) throws IllegalArgumentException {
		if(navn.length() == 0){throw new IllegalArgumentException("laegemiddler skal have et navn");}
		if(enhed.length() == 0){throw new IllegalArgumentException("laegemiddler skal have en enhed");}
		if(enhedPrKgPrDoegnLet <= 0){throw new IllegalArgumentException("laegemiddler skal have en anbefalet mængde");}
		if(enhedPrKgPrDoegnNormal <= 0){throw new IllegalArgumentException("laegemiddler skal have en anbefalet mængde");}
		if(enhedPrKgPrDoegnTung <= 0){throw new IllegalArgumentException("laegemiddler skal have en anbefalet mængde");}
		this.navn = navn;
		this.enhedPrKgPrDoegnLet = enhedPrKgPrDoegnLet;
		this.enhedPrKgPrDoegnNormal = enhedPrKgPrDoegnNormal;
		this.enhedPrKgPrDoegnTung = enhedPrKgPrDoegnTung;
		this.enhed = enhed;
	}
    

	public String getEnhed() {
		return enhed;
	}

	public String getNavn() {
		return navn;
	}

	public double getEnhePrKgPrDoegnLet(){
		return enhedPrKgPrDoegnLet;
	}
	
	public double getEnhePrKgPrDoegnNormal() {
		return enhedPrKgPrDoegnNormal;
	}
	
	public double getEnhePrKgPrDoegnTung() {
		return enhedPrKgPrDoegnTung;
	}
	
	@Override
    public String toString(){
		return navn;
	}
}
