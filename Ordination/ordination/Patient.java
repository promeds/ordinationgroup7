package ordination;

import java.util.ArrayList;

public class Patient {
	private String cprnr;
	private String navn;
	private double vaegt;
    private ArrayList<Ordination> ordinations = new ArrayList<>();

	public Patient(String cprnr, String navn, double vaegt) throws IllegalArgumentException {
			try{
				Integer.parseInt(cprnr.substring(0, 6));
				Integer.parseInt(cprnr.substring(7));
				if(cprnr.substring(7).length() != 4){
					throw new NumberFormatException();
				}
				
			}
			catch(NumberFormatException e){
				throw new IllegalArgumentException("please use a valid cprNr");
			}
            this.cprnr = cprnr;
            setNavn(navn);
            setVaegt(vaegt);
    }
	public String getCprnr() {
		return cprnr;
	}
	
	public String getNavn() {
		return navn;
	}
	
	public void setNavn(String navn) {
		if(navn.length() == 0){
			throw new IllegalArgumentException("Du skal have et navn");
		}
		this.navn = navn;
	}
	
	public double getVaegt(){
		return vaegt;
	}
	
	public void setVaegt(double vaegt)throws IllegalArgumentException{
		if (vaegt < 0){
            throw new IllegalArgumentException("Du kan ikke veje mindre end luft");
        }
        this.vaegt = vaegt;
	}

	@Override
    public String toString(){
		return navn + "  " + cprnr;
	}

    public ArrayList<Ordination> getOrdinationer(){
        return new ArrayList<>(ordinations);
    }

    public void addOrdination(Ordination ordination){
        ordinations.add(ordination);
    }



}
