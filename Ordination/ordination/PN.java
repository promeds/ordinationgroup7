package ordination;

import java.time.LocalDate;
import java.util.ArrayList;

public class PN extends Ordination{

	private double antalEnheder;
	private LocalDate firstDay;
	private LocalDate lastDay;
	private int timesGiven;
	
	public PN(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, double antalEnheder){
		super(startDen, slutDen, laegemiddel);
		this.antalEnheder = antalEnheder;
		timesGiven = 0;
	}
	
	
	/**
     * Registrerer at der er givet en dosis paa dagen givesDen
     * Returnerer true hvis givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
     * Retrurner false ellers og datoen givesDen ignoreres
     * @param givesDen
     * @return
     */
	public boolean givDosis(LocalDate givesDen) throws IllegalArgumentException {
        if(givesDen.isAfter(getStartDen().minusDays(1)) && givesDen.isBefore(getSlutDen().plusDays(1))){
        	if(firstDay == null){
        		firstDay = givesDen;
        		lastDay = givesDen;
        	} else{
        		if(firstDay.isAfter(givesDen)){
        			firstDay = givesDen;
        		}
        		if(lastDay.isBefore(givesDen)){
        			lastDay = givesDen;
        		}
        	}
        	timesGiven++;
        	return true;
        }
        throw new IllegalArgumentException("kan ikke gives uden for ordinations perioden");
	}
	
    public double doegnDosis() {
        if(timesGiven == 0){
        	return 0;
        } 
	    return timesGiven * antalEnheder / (firstDay.until(lastDay).getDays()+1);
	}


    public double samletDosis() {
        return timesGiven * antalEnheder;
    }

	/**
     * Returnerer antal gange ordinationen er anvendt
     * @return
     */
	public int getAntalGangeGivet() {
        return timesGiven;
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {
		return "pro necesare";
	}

}
